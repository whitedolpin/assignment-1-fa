﻿using Assigment1.Models;

namespace Assigment1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int size = 0;
            List<Teacher> teacherList = new List<Teacher>();
            int choice = 1;
            do
            {
                try
                {
                    Console.Clear();
                    Console.WriteLine("Teacher Management!!");
                    Console.WriteLine("****************************************");
                    Console.WriteLine("Please input the number of teacher: ");
                    size = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("****************************************");

                    Console.WriteLine("1. Input the information of teacher ");
                    Console.WriteLine("2. Show out the teacher with lowest salary");
                    Console.WriteLine("****************************************");
                    choice = Convert.ToInt32(Console.ReadLine());
                    switch (choice)
                {
                        case 1:
                            if (teacherList.Count >= size)
                            {
                                Console.WriteLine("The size is out of list!!!");
                            }
                            else
                            {
                                for (int i = 0; i < size; i++)
                                {
                                    Console.WriteLine("Ho ten:");
                                    string name = Console.ReadLine();

                                    Console.WriteLine("Nam sinh:");
                                    int namsinh = Convert.ToInt32(Console.ReadLine());

                                    Console.WriteLine("Luong co bann:");
                                    double luongCB = Convert.ToDouble(Console.ReadLine());

                                    Console.WriteLine("He so luong:");
                                    double HSluong = Convert.ToDouble(Console.ReadLine());

                                    Teacher teacher = new(name, namsinh, luongCB, HSluong);
                                    teacherList.Add(teacher);

                                    Console.WriteLine("Create Successfully");
                                }
                            }
                            break;
                        case 2:
                            if (teacherList.Count > 0)
                            {
                                Teacher lowstTeacher = teacherList.OrderBy(u => u.TinhLuong()).FirstOrDefault();
                                Console.WriteLine(lowstTeacher.XuatThongTin());
                            }
                            else
                            {
                                Console.WriteLine("The list is empty, please create a teacher");
                            }
                            break;
                        default:
                            Console.WriteLine("This choice is no supported");
                            break;
                    }
                    Console.ReadLine();
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Please try again! ");
                }
            } while (choice != 0);
        }
    }
}