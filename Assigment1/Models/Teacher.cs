﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assigment1.Models
{
    public class Teacher : Worker
    {
        public double HeSoLuong { get; set; }
        public Teacher() { }    
        public Teacher(string HoTen, int NamSinh,double LuongCoBan,double HeSoLuong)
        {
            this.Hoten = HoTen;
            this.NamSinh = NamSinh; 
            this.LuongCoBan = LuongCoBan;
            this.HeSoLuong = HeSoLuong; 
        }
        public void NhapThongTin(double hesoluong) => HeSoLuong = hesoluong;
        public double TinhLuong() => LuongCoBan * HeSoLuong * 1.25;
        public string XuatThongTin() => "Ho ten la: " + Hoten + ", Nam sinh: " + NamSinh
           + " Luong co ban: " + LuongCoBan + ", He so luong: " + HeSoLuong;
        public double XyLy() => HeSoLuong + 0.6;
    }
}
