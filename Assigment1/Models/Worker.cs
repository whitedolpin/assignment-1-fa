﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assigment1.Models
{
    public class Worker
    {
        public string Hoten { get; set; }   
        public int? NamSinh { get; set; }  
        public double LuongCoBan { get; set;}

        public Worker()
        {
            
        }

        public Worker(string Hoten, int NamSinh, double LuongCoBan)
        {
            this.Hoten = Hoten;
            this.NamSinh = NamSinh; 
            this.LuongCoBan = LuongCoBan;   
        }
        public Worker NhapThongTin(string Hoten, int namSinh, double luong) => new Worker
        {
            Hoten = Hoten,
            NamSinh = namSinh,
            LuongCoBan = luong
        };
        public double TinhLuong() => LuongCoBan;
        public String XuatThongTin() => "Ho ten la: " + Hoten + ", nam sinh: " + NamSinh
            + ", luong co ban: " + LuongCoBan;
    }
}
